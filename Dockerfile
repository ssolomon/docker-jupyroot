FROM rootproject/root:6.22.08-ubuntu20.04

# Installation needs to be done by the root user
USER root
ENV HOME /root

# Installing dependencies
RUN apt-get update && \
    apt-get install -y vim-tiny
    # Get java for ATLANTIS
    # apt-get install -y openjdk-11-jre-headless 


RUN apt-get install -y --no-install-recommends \
    python3-setuptools python3-pip sudo \
  && pip3 install jupyterlab metakernel \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

# Add Tini. Tini operates as a process subreaper for jupyter. This prevents kernel crashes.
# Read this for info: https://jupyter-notebook.readthedocs.io/en/stable/public_server.html#running-a-public-notebook-server
#ENV TINI_VERSION v0.6.0
##ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
#RUN chmod +x /tini
#ENTRYPOINT ["/tini", "--"]

# Setting language
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen

# Creating user as fp_student
ENV SHELL=/bin/bash \
    FP_USER=fp_student \
    FP_UID=1000 \
    LC_ALL=en_US.UTF-8 \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US.UTF-8

# Adding the user to the system
RUN useradd -m -s /bin/bash -u $FP_UID $FP_USER

# RUN echo "fp_student ALL=(ALL:ALL) NOPASSWD: ALL" > /etc/sudoers.d/fp_student # try without first

USER $FP_UID
ENV HOME /home/$FP_USER
RUN mkdir /home/$FP_USER/work
RUN mkdir /home/$FP_USER/.jupyter
RUN jupyter notebook --generate-config
#Creating the configuration file for jupyter
RUN echo "c.NotebookApp.extra_static_paths = [\"$ROOTSYS/js/\"]" >> /home/$FP_USER/.jupyter/jupyter_notebook_config.py

#Setting the default directory in the container
WORKDIR /home/$FP_USER/work

# Setting the port of the container. 
EXPOSE 8888 
# By default, the notebook server runs locally at 127.0.0.1:8888
# To use another port, try root --notebook --ip=0.0.0.0 --port <your_port> in the command line. 
CMD ["root", "--notebook", "--ip=0.0.0.0"]
